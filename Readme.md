# このサンプルについて

## 概要

スクイークの実習用テキストである「すくすくスクイーク」の「車の運転」の教材を，スクイークEtoys5.0+日本語修正パッケージ2013年度版の組み合わせで利用できるように，図などを一部変更したものです．

## ダウンロード

手っ取り早く利用したい方は，以下のURLよりPDFファイルをダウンロードしてください．

<https://bitbucket.org/nmiri_nagoya_nsaito/drive_a_car-etoys5.0j/downloads/drive_a_car-etoys5.0j.pdf>

それ以外にこのリポジトリにはPDF作成元のWordデータ，画像データ，プロジェクトデータなどを置いています．

## オリジナルの資料

オリジナルのすくすくスクイークは以下から入手できます．

<http://squeakland.jp/sqsqsqueak/>

## スクイークEtoysの入手

スクイークEtoysおよび日本語修正パッケージは以下から入手できます．

<http://etoys.jp/squeak/squeak.html>

## その他

元資料からの変更箇所について，間違い等ありましたらご連絡ください．

<saito.naoki@nmiri.city.nagoya.jp>